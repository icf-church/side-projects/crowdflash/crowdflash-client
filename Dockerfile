FROM node

RUN npm install -g @vue/cli @vue/cli-service-global

ENTRYPOINT [ "npm", "run", "serve" ]