import { createApp } from 'vue/dist/vue.esm-bundler';
import { createRouter, createWebHistory } from 'vue-router';
import NoSleep from 'nosleep.js';
import App from './App.vue';
import Switcher from './Switcher.vue';

const router = createRouter({
  history: createWebHistory(process.env.VUE_APP_HISTORY_BASE),
  routes: [
    {
      path: '/',
      name: 'switcher',
      component: Switcher,
    },
    {
      path: '/:id',
      component: App,
      name: 'canvas',
      props: true,
    },
  ],
});

const app = createApp({});

app.use(router);

const noSleep = new NoSleep();
app.provide('noSleep', noSleep);

app.mount('#app');
