# Crowdflash

Crowdflash is an application that allows displaying colors on many smartphones and tablets at the same time, controlled via DMX interfaces.

Crowdflash consists of four main components that interact with one another in real time.

- Crowdflash Bridge
  - reads input via [ArtNet](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-artnet-bridge) or [sACN](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-sacn-bridge) and publishes updates to Redis
- Redis Pub/Sub Server
- [Crowdflash Websocket Server](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-pubsub-server)
  - subscribes to Redis and pushes updates to all connected websocket clients
- [Crowdflash Client](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-client)
  - displays the received colors on the screen

# Crowdflash Client

The Crowdflash Client displays the color received by the websocket server. To enable the best effect, it shows a fullscreen button and requests a device wakelock.


## Installation
```
npm ci
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

## Production Hosting

We would recommend hosting the site with any of the many readily available static site hosters. Cheap and easy to set up
options include GitHub / GitLab Pages, DigitalOcean App Platform or even Cloudflare.

This allows for flexible scaling, simple deployment and powerful hosting features like a distributed hosting via CDNs.

We are using the DigitalOcean App Platform to host our production environment. Feel free to take a look and try it out
yourself!

[![Deploy to DO](https://www.deploytodo.com/do-btn-blue.svg)](https://cloud.digitalocean.com/apps/new?repo=https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-client/tree/master&refcode=2c2a7cfa1ce3)

Please note that this app currently doesn't support connecting to redis via TLS, which is required for using the
DigitalOcean Managed Redis Clusters. If you need this functionality, feel free to open an issue or just create a pull
request!

You will also (most probably) need to host the Crowdflash Bridge locally in your own network so that it can receive the
UDP data from the sACN or ArtNet source device.

## Business requests

If you would like to use Crowdflash at your event, but don't feel like setting up and managing the required
infrastructure yourself, we would be more than happy to talk to you! Feel free to reach out to us via email
at [web@icf.ch](mailto:web@icf.ch)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/) - see [LICENSE](LICENSE) file


